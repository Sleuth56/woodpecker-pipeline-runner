import json
import requests
import toml
import logging
import sys
import re
import os

_LOGGER = logging.getLogger(__name__)
stream = logging.StreamHandler(sys.stdout)
stream.setLevel(logging.DEBUG)
_LOGGER.addHandler(stream)

regex_git='https://(.*)/(.*)/(.*)'
regex_site='https://(.*)/(.*)'
# CI API Paths
url_get_builds='{domain}/api/repos/{owner}/{repo}/builds'
url_create_build='{domain}/api/repos/{owner}/{repo}/builds/{number}'
# Git forge API Paths
url_gh_release='https://github.com/{owner}/{repo}/releases.atom'
url_gl_release='https://{domain}/api/v4/projects/{owner}%2F{repo}/releases'
url_gt_release='https://{domain}/api/v1/repos/{owner}/{repo}/releases'

def start_build(domain, owner, repo, token, job_number='latest'):
  if (job_number=='latest'):
    try:
      print(f"Starting job {url_get_builds.format(domain=domain, owner=owner, repo=repo)}")
      resp=requests.get(url_get_builds.format(domain=domain, owner=owner, repo=repo, number=''), headers={"Authorization":f"Bearer {token}"})
      job_number=resp.json()[0]['number']
    except:
      return 'Failed to obtain build number'

  _LOGGER.debug(job_number)
  try:
    print(f"Starting job {url_create_build.format(domain=domain, owner=owner, repo=repo, number=job_number)}")
    requests.post(url_create_build.format(domain=domain, owner=owner, repo=repo, number=job_number), headers={"Authorization":f"Bearer {token}"})
    return 'Job started'
  except:
    return 'Failed to start job'

# Update
def check_github(config_data, domain, token, job_number='latest'):
  for count in range(len(config_data)):
    gh_repo=list(config_data.keys())[count]
    ci_path=config_data[list(config_data.keys())[count]]
    parsed_repo_url=re.findall(regex_git, gh_repo)[0]
    print(f"Checking {gh_repo}")

    resp=requests.get(f'{gh_repo}/releases.atom')
    path=f'data/github/{parsed_repo_url[1]}'

    # Create project directory if it doesn't exist
    if (os.path.isfile(f'{path}/{parsed_repo_url[2]}') == False):
      os.makedirs(path, exist_ok=True)
      with open(f'{path}/{parsed_repo_url[2]}', 'x') as fp:
        pass
      print('No history file. Creating...')
    else:
      # Launch CI if the release file doesn't match what was downloaded
      with open(f'{path}/{parsed_repo_url[2]}', 'r') as file:
        data=file.read()
        if (data == resp.text):
          print("No updates. Skipping...")
        else:
          start_build(domain, ci_path.split('/')[0], ci_path.split('/')[1], token, job_number)
          print("Update found. Launched CI.")

    # Write new data to file
    with open(f'{path}/{parsed_repo_url[2]}', 'w') as file:
      file.write(resp.text)

def check_website(config_data, domain, token, job_number='latest'):
  for count in range(len(config_data)):
    site=list(config_data.keys())[count]
    ci_path=config_data[list(config_data.keys())[count]]
    parsed_repo_url=re.findall(regex_site, site)[0]
    print(f"Checking {site}")

    resp=requests.get(f'{site}')
    path=f'data/site/{parsed_repo_url[0]}'

    # Create project directory if it doesn't exist
    if (os.path.isfile(f'{path}/{parsed_repo_url[1]}') == False):
      os.makedirs(path, exist_ok=True)
      with open(f'{path}/{parsed_repo_url[1]}', 'x') as fp:
        pass
      print('No history file. Creating...')
    else:
      # Launch CI if the release file doesn't match what was downloaded
      with open(f'{path}/{parsed_repo_url[1]}', 'r') as file:
        data=file.read()
        if (data == resp.text):
          print("No updates. Skipping...")
        else:
          start_build(domain, ci_path.split('/')[0], ci_path.split('/')[1], token, job_number)
          print("Update found. Launched CI.")

    # Write new data to file
    with open(f'{path}/{parsed_repo_url[1]}', 'w') as file:
      file.write(resp.text)

if __name__ == '__main__':
  _LOGGER.setLevel(logging.DEBUG)
  with open('config.toml', 'r') as config_file:
    config=toml.loads(config_file.read())
    print("Checking Github")
    check_github(config['Github'], config['ci_url'], config['token'])
    print("Checking WebSites")
    check_website(config['WebSite'], config['ci_url'], config['token'])
    # print("Checking Gitlab")
    # check_github(config['Gitlab'], config['ci_url'], config['token'])
    # print("Checking Gitea")
    # check_github(config['Gitea'], config['ci_url'], config['token'])